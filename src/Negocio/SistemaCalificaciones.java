/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Util.LeerMatriz_Excel;
import java.io.IOException;

/**
 *  Clase del negocio para la manipulación de una lista de Calificaciones
 * @author Hernando Zuluaga
 */
public class SistemaCalificaciones {
    
    private Estudiante []listaEstructuras;

    public SistemaCalificaciones() {
    }
    
    
    public SistemaCalificaciones(String nombreArchivo) throws IOException {
        LeerMatriz_Excel miExcel=new LeerMatriz_Excel(nombreArchivo,0);
        String miMatriz[][]=miExcel.getMatriz();
        
        //Normalizar: Pasar de un archivo a un modelo de objetos
        this.listaEstructuras=new Estudiante[miMatriz.length-1];
        crearEstudiantes(miMatriz);
        
        
    }
    
    
    private void crearEstudiantes(String datos[][])
    {
    
        for(int fila=1;fila<datos.length;fila++)
        {
            //Datos para un estudiante
            String nombre="";
            long codigo=0;
            float quices[]=new float[datos[fila].length-2];
            int indice_quices=0;
            for(int columna=0;columna<datos[fila].length;columna++)
            {
            
                
                if(columna==0)
                    codigo=Long.parseLong(datos[fila][columna]);
                else
                {
                    if(columna==1)
                        nombre=datos[fila][columna];
                    else
                    {
                        quices[indice_quices]=Float.parseFloat(datos[fila][columna]);
                        indice_quices++;
                    }
                }
                
            }
            //Creó al estudiante
            Estudiante nuevo=new Estudiante(codigo, nombre);
            nuevo.setQuices(quices);
            //Ingresar al listado de Estudiantes:
            this.listaEstructuras[fila-1]=nuevo;
            
        
        }
          
        
    }
    
    
     public SistemaCalificaciones(int can) {
         
         // T identi[] = new T[tamaño];
         //T es Estudiante
            this.listaEstructuras=new Estudiante[can];
    }
    
    
     //Esto va a cambiar
     public void insertarEstudiante_EnPos0(long codigo,String nombre,  String notas)
     {
         Estudiante nuevo=new Estudiante(codigo, nombre, notas);
         
         this.listaEstructuras[0]=nuevo;
         
     }

    @Override
    public String toString() {
        
        String msg="Mis estudiantes son:\n";
        
        for(Estudiante unEstudiante:this.listaEstructuras)
            msg+=unEstudiante.toString()+"\n";
        
        return msg;
        
    }
     
     
    /**
     * Obtiene los estudiantes cuyo promedio de quices es menor a 3
     * @return un vector de objetos de la clase Estudiante
     */
    public Estudiante[] getReprobaronQuices()
    {
        int j=0; 
        int aux= hayarEstudiantes("menor");
        Estudiante []pierden= new Estudiante[aux];
        
        for (int i = 0; i < this.listaEstructuras.length; i++) {
          float prom = getPromedio(listaEstructuras[i]); 
           if(prom<3){
              pierden[j]= listaEstructuras[i];
              j++; 
           }
        }
    
        return pierden;
    }
    
    /**
     * Obtiene los estudiantes cuyo promedio de quices es mayor o igual a 4
     * @return un vector de objetos de la clase Estudiante
     */
    public Estudiante[] getMayorNotaQuices()
    {
        int j=0;
        int aux= hayarEstudiantes("mayor");
        Estudiante []mayores= new Estudiante[aux];
        
        for (int i = 0; i < this.listaEstructuras.length; i++) {
          float prom = getPromedio(listaEstructuras[i]); 
           if(prom>=4){
              mayores[j]= listaEstructuras[i];
              j++; 
           }
        }
    
        return mayores;
    }
    
    /**
     * Obtiene el nombre del quiz que más perdieron los estudiantes (q1, q2....qn)
     * @return un String con el nombre de la columna en Excel
     */
    public String getNombreQuiz_Perdieron()
    {   
        int i=0;
        String msg="";
        int contador=0;
        int quiz=0;
        int contadorQuiz=0;
        
        Estudiante []estudiantes= this.listaEstructuras;
        float []quices= this.listaEstructuras[i].getQuices();
        for(int j=0; j<quices.length; j++){
        
            for(i=0; i< estudiantes.length; i++){
              
                if(quices[j]<3)
                    {
                       contador++;         
                    }
            }
            if(contador>contadorQuiz){
                quiz=j+1;
                contadorQuiz=contador;
                msg="QUIZ "+(quiz); 
            }  
        }
     return msg;
    }
    
    
    /**
     * Obtiene la nota que más se repite  (Ojo suponga notas de un entero y un decimal
     * @return un float con la nota que más se repite
     */
    public float getNota_Que_MasRepite()
    {
        float notaMasRepetida=0;
        int contadorResultado=0;
        
        for(float nota=0; nota<=5; nota+=0.1){
            int contador=0;
            
            for (int i = 0; i < this.listaEstructuras.length; i++) {
                
                for (int j = 0; j < this.listaEstructuras[i].getQuices().length; j++) {
                    float aux= this.listaEstructuras[i].getQuices()[j];
                    if(nota==aux)contador++;
                        
                }               
            }
            if(contador>contadorResultado){
                notaMasRepetida=nota;
                contadorResultado=contador;
            }
        }
    
        return notaMasRepetida;
    }
    
    /**
     * Obtiene el promedio del estudiante 
     * @param estudiante
     * @return 
     */    
    public float getPromedio(Estudiante estudiante)
    {
        float []arreglo;
        arreglo = estudiante.getQuices();
        
        float prom=0;
        
        for (int i = 0; i < arreglo.length; i++) 
          prom= prom + arreglo[i];  
        
        return prom/arreglo.length;
    }
    
    /**
     * Obtiene el numero de estudiantes que han ganado o perdido segun se requiera
     * Podemos decirle por medio de un String que queremos hallar 
     * dato==mayor; haya el numero de estudiantes con promedio mayor o igual a 4
     * dato==menor;  haya el numero de estudiantes con promedio menor a 3
     * @param dato
     * @return numero de estudiantes segun lo que le indiquemos
     */
   
    public int hayarEstudiantes(String dato)
    {
        
        int contador=0;
        if (dato=="menor"){
            for (int i = 0; i < this.listaEstructuras.length; i++) {
                float aux= getPromedio(this.listaEstructuras[i]);
                if(aux<3) contador++;                
            }
        }
       
        
        if (dato=="mayor"){
            for (int i = 0; i < this.listaEstructuras.length; i++) {
                float aux= getPromedio(this.listaEstructuras[i]);
                if(aux>=4) contador++;                
            }
        }
        return contador;
    }
    
}
