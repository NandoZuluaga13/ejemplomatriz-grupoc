/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import Negocio.SistemaCalificaciones;
import java.io.IOException;

/**
 *
 * @author Hernando Zuluaga
 */
public class PruebaExcelSistemaCalificacion {
    
    public static void main(String[] args) throws IOException {
        SistemaCalificaciones sistema=new SistemaCalificaciones("src/Datos/estudiantes.xls");
        //System.out.println(sistema.toString());
        
        Estudiante []mayor= sistema.getMayorNotaQuices();
        System.out.println("MAYORES NOTAS");
        for (int i = 0; i < mayor.length; i++) 
        {
            System.out.println(mayor[i].toString());
        }
        
        Estudiante []menor= sistema.getReprobaronQuices();
        System.out.println("REPROBARON QUICES");
        for (int i = 0; i < menor.length; i++) 
        {
            System.out.println(menor[i].toString());
        }
        
        float nota = sistema.getNota_Que_MasRepite();
        System.out.println("LA NOTA QUE MAS SE REPITE ES: "+nota);
        
        String quiz= sistema.getNombreQuiz_Perdieron();
        System.out.println("EL QUIZ QUE MAS ESTUDIANTES PERDIERON FUE EL "+quiz);
        }
   }
